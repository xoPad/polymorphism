/*
    1.Виртуальные функции (переопределённые методы)
    Выполнение наиболее дочернего метода. Без virtual будет вызываться
   родительская функция
*/

#include <iostream>

class Animal {
   protected:
    int m_age = 0;
    std::string m_name;
    std::string m_color;

   public:
    Animal(int age, std::string name, std::string color)
        : m_age(age), m_name(name), m_color(color) {}

    std::string getName() const { return m_name; }
    std::string getColor() const { return m_color; }
    int getAge() const { return m_age; }
    // 1. Говорим, что она виртуальная, для возможности переопределения в
    // дочерних классах
    virtual std::string printInformation() { return "Information: Animal"; }

    ~Animal() { std::cout << "Animal ..." << std::endl; }
};

class Dog : public Animal {
   public:
    Dog(int age, std::string name, std::string color)
        : Animal(age, name, color) {}

    ~Dog() { std::cout << "Dog ..." << std::endl; }
    // 1. Переопределение виртуальной функции
    std::string printInformation() override { return "Information: Dog"; }
};

int main(int argc, char const *argv[]) {
    Dog dog(10, "Gg", "rgb");
    Animal &animal = dog;

    std::cout << animal.getAge() << " " << animal.getName() << " "
              << animal.getColor() << std::endl;

    // 1. Использование, если убрать виртуал у родительского класса, то будет
    // выводить Information: Animal, если виртуал оставить, то Information: Dog
    std::cout << animal.printInformation() << std::endl;

    // Dog dog(10, "Gg", "rgb");
    // Animal *animal = &dog;

    // std::cout << animal->getAge() << " " << animal->getName() << " "
    //           << animal->getColor() << std::endl;

    // 1. Virtual function
    // std::cout << animal->printInformation() << std::endl;

    return 0;
}
