/*
    4. Перегрузка операторов + << >> == !=
*/

#include <iostream>

class Animal {
   protected:
    int m_age = 0;
    std::string m_name;
    std::string m_color;

   public:
    Animal(int age, std::string name, std::string color)
        : m_age(age), m_name(name), m_color(color) {
        // std::cout << "Animal!" << std::endl;
    }

    std::string getName() const { return m_name; }
    std::string getColor() const { return m_color; }
    int getAge() const { return m_age; }

    virtual ~Animal() {
        // std::cout << "Animal ..." << std::endl;
    }
};

class Dog : public Animal {
   public:
    Dog(int age, std::string name, std::string color)
        : Animal(age, name, color) {
        // std::cout << "Dog!" << std::endl;
    }

    // Выполняем Dog + Dog через дружественную функцию
    friend Dog operator+(const Dog &lhs, const Dog &rhs);

    friend std::ostream &operator<<(std::ostream &stream, const Dog &dog) {
        stream << "Age: " << dog.getAge() << std::endl;
        stream << "Name: " << dog.getName() << std::endl;
        stream << "Color: " << dog.getColor() << std::endl;
        return stream;
    }

    ~Dog() {
        // std::cout << "Dog ..." << std::endl;
    }
};

// Эта функция не является методом класса!
Dog operator+(const Dog &lhs, const Dog &rhs) {
    // Используем конструктор Dog и operator+(int, int).
    // Мы имеем доступ к закрытым членам m_age, m_name, m_color, поскольку эта
    // функция является дружественной классу Dog
    return Dog(lhs.m_age + rhs.m_age, lhs.m_name + rhs.m_name,
               lhs.m_color + rhs.m_color);
}

// Перегрузка вне класса
bool operator==(const Dog &lhs, const Dog &rhs) {
    return lhs.getAge() == rhs.getAge() && lhs.getColor() == rhs.getColor() &&
           lhs.getName() == rhs.getName();
}

int main(int argc, char const *argv[]) {
    // Проверка оператора сравнения
    Dog dog1(32, "Kooks", "green");
    Dog dog2(23, "Lols", "red");

    if (dog1 == dog2) {
        std::cout << dog1.getName() << " is " << dog2.getName() << std::endl;
    } else {
        std::cout << dog1.getName() << " is not " << dog2.getName()
                  << std::endl;
    }

    std::cout << std::endl;

    // Проверка дружественного оператора сложения перегруженного вне класса
    Dog dog3 = dog1 + dog2;
    std::cout << "dog1 + dog2 Age - " << dog3.getAge() << std::endl;
    std::cout << "dog1 + dog2 Name - " << dog3.getName() << std::endl;
    std::cout << "dog1 + dog2 Color - " << dog3.getColor() << std::endl;

    std::cout << std::endl;

    // Проверка дружественного оператора вывода перегруженного в классе
    std::cout << dog3 << std::endl;
    return 0;
}
