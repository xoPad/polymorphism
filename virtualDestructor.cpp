/*
    2.Виртуальные деструкторы
    Деструктор полиморфного базового класса должен объявляться виртуальным.
   Только так обеспечивается корректное разрушение объекта производного класса
   через указатель на соответствующий базовый класс
*/

#include <iostream>

class SomeObject {
   public:
    SomeObject() { std::cout << "SomeObject!" << std::endl; };
    ~SomeObject() { std::cout << "SomeObject ..." << std::endl; };
};

class Animal {
   protected:
    int m_age = 0;
    std::string m_name;
    std::string m_color;

   public:
    Animal(int age, std::string name, std::string color)
        : m_age(age), m_name(name), m_color(color) {
        std::cout << "Animal!" << std::endl;
    }

    std::string getName() const { return m_name; }
    std::string getColor() const { return m_color; }
    int getAge() const { return m_age; }

    // 2. Если убрать виртуал, то удаление Dog и SomeObject не выполнится
    virtual ~Animal() { std::cout << "Animal ..." << std::endl; }
};

class Dog : public Animal {
   public:
    Dog(int age, std::string name, std::string color)
        : Animal(age, name, color) {
        std::cout << "Dog!" << std::endl;
    }

    ~Dog() { std::cout << "Dog ..." << std::endl; }
    // Пример другого объекта доя наглядности
    SomeObject object;
};

int main(int argc, char const* argv[]) {
    Animal* animal = new Dog(10, "Gg", "rgb");
    delete animal;

    return 0;
}
