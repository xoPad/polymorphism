/*
    1.Виртуальные функции (переопределённые и не переопределённые методы) пример
   2
*/

#include <iostream>

class Animal {
   public:
    std::string Speak() { return "Animal: Non-virtual ..."; };
    virtual std::string virtualSpeak() { return "Animal: Virtual ..."; };
};

class Dog : public Animal {
   public:
    std::string Speak() { return "Dog: Non-virtual woof"; };

    // Кто-то использует с обозначением virtual
    // virtual std::string virtualSpeak() { return "Dog: Virtual woof"; };

    // Кто-то без обозначения какого-либо, но нечитаемо
    // std::string virtualSpeak() { return "Dog: Virtual woof"; };

    // Так понятнее с override
    std::string virtualSpeak() override { return "Dog: Virtual woof"; };
};

int main(int argc, char const *argv[]) {
    Animal animal;
    Dog dog;

    Animal *test = &animal;
    std::cout << test->Speak() << std::endl;
    std::cout << test->virtualSpeak() << std::endl;

    test = &dog;
    std::cout << test->Speak() << std::endl;
    std::cout << test->virtualSpeak() << std::endl;

    return 0;
}
